var express = require('express');
var router = express.Router();
var mysql = require('mysql')
var querySQL = `SELECT post.id, post.title, post.short_desc, post.content, post.image, post.author, post.category, category.name FROM post INNER JOIN category ON post.category = category.id`;
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'mob402_ass'
});
connection.connect();

var multer = require('multer');
const ejs = require('ejs');
const path = require('path');

// <!--=========================================Set The Storage Engine======================================================-->
const storage = multer.diskStorage({
  destination: './public/images/',
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

// <!--=========================================Init Upload======================================================-->
const upload = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  }
}).single('image');

// <!--=========================================Check File Type======================================================-->
function checkFileType(file, cb) {
  const filetypes = /jpeg|jpg|png|gif/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Cảnh báo: Chỉ được chọn ảnh! Hãy chọn lại!');
  }
}

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('./public'));

// <!--========================================= GET home page. ======================================================-->
router.get('/', function (req, res, next) {
  const sql = `${querySQL}`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err
    connection.query('SELECT * FROM category', function (err, row, fields) {
      if (err) throw err
      res.render('index', { title: 'TIN NÓNG TRONG NGÀY', data: rows, category: row });
    })
  })
});

// <!--========================================= GET detal category page.======================================================-->
router.get('/detailCategory/:id/', function (req, res, next) {
  const sql = `${querySQL} and post.category = '${req.params.id}'`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err
    connection.query('SELECT * FROM category', function (err, row, fields) {
      if (err) throw err
      res.render('detailCategory', { title: 'Post', data: rows, category: row, id: req.params.id });
    })
  })
});

// <!--=========================================GET detal Post page.======================================================-->
router.get('/detailPost/:id', function (req, res, next) {
  const sql = `${querySQL} and post.id = '${req.params.id}'`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err
    res.render('detailPost', { title: 'Detail', data: rows });
  })
});

// <!--=========================================GET detal Edit page.======================================================-->
router.get('/edit/:id', function (req, res, next) {
  const sql = `${querySQL} AND post.id = ${req.params.id}`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err
    connection.query('SELECT * FROM category', function (err, row, fields) {
      if (err) throw err
      res.render('edit', { title: 'Edit', data: rows, categoryDATA: row });
    })
  })
})

router.post('/editpost/:id', function (req, res, next) {
  upload(req, res, (err) => {
    if (err) {
      res.send(err);
    } else {
      if (req.file == undefined) {
        const sql = `UPDATE post
         SET title='${req.body.title}',
         short_desc='${req.body.short_desc}',
         content='${req.body.content}',
         author='${req.body.author}',
         category='${req.body.category}'
         WHERE ID = '${req.params.id}'`
        connection.query(sql, function (err, rows, fields) {
          if (err) throw err;
          res.redirect('/')
        });
      } else {
        var image = 'images/' + req.file.filename;
        var sql = `UPDATE post
  				SET 
  				title = '${req.body.title}', 
  				short_desc = '${req.body.short_desc}', 
  				image = '${image}', 
  				content = '${req.body.content}', 
  				author = '${req.body.author}',
  				category = '${req.body.category}'
  				WHERE ID =  '${req.params.id}'`;
        connection.query(sql, function (err, rows, fields) {
          if (err) throw err;
          res.redirect('/')
        });
      }
    }
  });
});

// <!--=========================================GET Post page.======================================================-->
router.get('/post', function (req, res) {
  connection.query('SELECT * FROM category', function (err, rows, fields) {
    if (err) throw err
    res.render("post", { title: 'Post', data: rows })
  })
});

router.post('/savePost', function (req, res, next) {
  upload(req, res, (err) => {
    if (err) {
      res.send(err);
    } else {
      if (req.file == undefined) {
        res.send('no file select');
      } else {
        var image = 'images/' + req.file.filename;
        var sql = `INSERT into post (title, short_desc, image, content, author, category) 
          values (
            '${req.body.title}',
            '${req.body.short_desc}',
            '${image}',
            '${req.body.content}',
            '${req.body.author}',
            '${req.body.category}'
          )`;
        connection.query(sql, function (err, rows, fields) {
          if (err) throw err;
          res.redirect('/')
        });
      }
    }
  });
});

// <!--=========================================GET Remove post======================================================-->
router.get('/remove/:id', function (req, res, next) {
  var id = req.params.id;
  var sql = `DELETE from post where id = ${id}`;
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.redirect('/');
  });
});

// <!--=========================================GET add category.======================================================-->
router.post('/addCategory/:name', function (req, res, next) {
  var name = req.params.name;
  var sql = `INSERT INTO category (name, images) 
  values (
    '${name}',
    '${'https://cailuongmp3.com/getmedia/e0eb825c-04ae-45be-b6bd-a64e49e99a6f/Trich-%C4%90oan-Tan-Co-Tuyen-Chon-Hay-Nhat-Minh-Phung-Le-Thuy.aspx?ext=.jpg'}'
  )`;
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.redirect('/');
  });
});

// <!--=========================================GET edit category.======================================================-->
router.post('/editCategory/:id/:name', function (req, res, next) {
  var name = req.params.name;
  var id = req.params.id;
  console.log(name + id)
  var sql = `UPDATE category SET name = '${name}' WHERE id = '${id}'`;
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.redirect('/');
  });
});

// <!--=========================================GET delete category.======================================================-->
router.post('/deleteCategory/:id', function (req, res, next) {
  var id = req.params.id;
  var sql = `DELETE from category where id = ${id}`;
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.redirect('/');
  });
});

// <!--=========================================API======================================================-->
router.get('/API/POST/:id', function (req, res, fields) {
  var sql = (Number(req.params.id) === 0) ? `SELECT * FROM POST` : `${querySQL} and post.category = '${req.params.id}'`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.json(rows)
  })
})

router.get('/API/CATEGORY', function (req, res, fields) {
  var sql = `SELECT * FROM CATEGORY`
  connection.query(sql, function (err, rows, fields) {
    if (err) throw err;
    res.json(rows)
  })
})
module.exports = router;
